Rails.application.routes.draw do
  devise_for :users
	root 'welcome#homepage'
  	get 'welcome/homepage'
    get '/pokemons', to: 'pokemon#index'
    post '/pokemons', to: 'pokemon#create'
    get '/pokemons/:id', to: 'pokemon#show'

  	#root 'welcome#login'
  	#get 'welcome/login'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
