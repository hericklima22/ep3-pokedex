require 'test_helper'

class PokemonControllerTest < ActionDispatch::IntegrationTest
  test "should get random:integer" do
    get pokemon_random:integer_url
    assert_response :success
  end

  test "should get imgurl:string" do
    get pokemon_imgurl:string_url
    assert_response :success
  end

end
